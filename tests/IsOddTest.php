<?php

require_once  __DIR__ . '/../vendor/autoload.php';

use IsOdd\IsOdd;

class Test extends PHPUnit_Framework_TestCase
{

    /**
     * Asserts that the function recognizes oddness properly
     */

    public function  testTruncation()
    {
        $this->assertEquals(true, IsOdd::isOdd(3));
        $this->assertEquals(false, IsOdd::isOdd(12));
    }
}
