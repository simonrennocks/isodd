<?php

namespace IsOdd;

use Couchbase\BooleanFieldSearchQuery;
use http\Message\Body;

class IsOdd
{
    public  static function isOdd(int $number):bool
    {
        return $number % 2 === 1;
    }
}